#!/usr/bin/env node

const _ = require('lodash');
const fs = require('fs');
const path = require('path');
const program = require('commander');
const { execSync } = require('child_process');

// const schema = require('../lib/testDataSchema');
const environment = require('../lib/environment')();

// Helper functions

const exec = _.partial(execSync, _, { stdio: [0, 1, 2] });

// Program configuration

program.version('1.0.0');

const defaults = {
  test: _.defaults({}, environment, {
    input: path.resolve(__dirname, '..', 'src', 'index.js'),
    csvReport: path.resolve(__dirname, '..', 'report.csv'),
    dataPath: path.resolve(__dirname, '..', 'testData.json')
  }),
  deploy: {
    domain: 'myir-test-report.surge.sh',
    project: path.resolve(__dirname, '..', 'mochawesome-report')
  },
  doc: {
    input: path.resolve(__dirname, '..', 'src'),
    to: 'commonmark'
  }
};

// Subcommands

function monkeypatchTestEnvironment(options) {
  if (options.enableDebug !== undefined) {
    process.env.ENABLE_DEBUG = options.enableDebug;
  }
  if (options.chromeOpts !== undefined) {
    process.env.CHROME_OPTS = options.chromeOpts;
  }
  if (options.shortTimeout !== undefined) {
    process.env.SHORT_TIMEOUT = options.shortTimeout;
  }
  if (options.timeout !== undefined) {
    process.env.TIMEOUT = options.timeout;
  }
  if (options.longTimeout !== undefined) {
    process.env.LONG_TIMEOUT = options.longTimeout;
  }
  if (options.failedScreenshotPath !== undefined) {
    process.env.FAILED_SCREENSHOT_PATH = options.failedScreenshotPath;
  }
  if (options.testPattern !== undefined) {
    process.env.TEST_PATTERN = options.testPattern;
  }
}

function createCsvReport(options) {
  const jsonReportPath = path.resolve(
    __dirname, '..', 'mochawesome-report', 'index.json'
  );
  if (fs.existsSync(jsonReportPath)) {
    let jsonReport;
    try {
      jsonReport = require(jsonReportPath);
    } catch(e) {
      throw 'Could not open JSON report.';
    }
    let headers = ['tests', 'passes', 'failures', 'pending'];
    let data = [];
    headers.forEach((header) => (
      data.push(_.get(jsonReport, ['stats', header], 0))
    ));
    headers = headers.join(',');
    data = data.join(',');
    try {
      fs.writeFileSync(options.csvReport, `${headers}\n${data}`);
    } catch(e) {
      throw 'Could not write to CSV file.';
    }
  }
}

function buildReportTitle(options) {
  if (options.reportTitle !== undefined) {
    return options.reportTitle;
  }
  if (process.env.REPORT_TITLE !== undefined) {
    return process.env.REPORT_TITLE;
  }

  let gitInfo;
  try {
    const gitCmd = `git --git-dir "${path.resolve(__dirname, '..', '.git')}"`;
    let tags = '';
    let commit = '';
    let dirty = '';

    tags = execSync(`${gitCmd} tag -l --points-at HEAD`).toString();

    if (tags === '') {
      commit = execSync(`${gitCmd} rev-parse --short HEAD`);
    }

    if (execSync(`${gitCmd} diff --shortstat`).toString() !== '') {
      dirty = ' (dirty)';
    }

    gitInfo = ` ${tags}${commit}${dirty}`;
    // replace newlines/multiple spaces with a single space
    gitInfo = gitInfo.replace(/(\n| )+/g, ' ');
    // trim spaces at the end
    gitInfo = gitInfo.replace(/ $/, '');
  } catch(e) {
    gitSubStr = '';
    console.error('Could not retrieve git information for report title, some information may be incomplete');
  }

  return `MYIR UI Regression Tests: ${gitInfo}`;
}

function setupInteractiveMode() {
  // TODO: these technically belong in lib
  const { Driver } = require('../src/core');
  const { LoginFlow } = require('../src/flows');
  const data = require('../src/data').userData;
  global.driver = Driver();
  global.loginFlow = new LoginFlow({ driver, testCase: { url: data[0].url } });
  require('../lib/interactive')(global.driver);
}

program
  .command('test')
  .description('Runs the tests through mocha.')
  .option('-i, --input [path]', 'The test entrypoint. Defaults to ./src/index.js')
  .option('--chrome-opts [options]', 'An array of comma-delimited extra options to pass to chromedriver.')
  .option('--interactive', 'Instead of running tests, enters into an interactive mode with the test library imported and logged in. Invalidates all options below')
  .option('--enable-debug', 'Enable pausing and entering interactive mode when a test fails. Prevents mocha from exiting if there are any pending operations')
  .option('--short-timeout [ms]', 'How long to wait for short tests')
  .option('--timeout [ms]', 'How long to wait for average tests')
  .option('--long-timeout [ms]', 'How long to wait for long tests')
  .option('--failed-screenshot-path [path]', 'The path of the folder to save screenshot failures. Defaults to ./mochawesome-report/failed')
  .option('--test-pattern [regex]', 'A pattern used to filter full test names (including the suite names)')
  .option('--report-title', 'The report title to use in --build mode. Defaults to "MyIR UI Regression Tests: (git info)"')
  .option('--csv-report [path]', 'The path to export a CSV report in --build mode. Defaults to ./report.csv')
  .option('--build', 'Build a deployable html report. Invalidates all options below')
  .option('--no-watch', 'Do not watch the source files for changes')
  .option('--bail', 'Will stop running tests once they fail')
  .option('--until-failure', 'Runs the tests until they fail. This disables watch mode.')
  .option('-r, --reporter [mocha reporter]', 'The reporter to use. Passed to mocha if specified')
  .action((rawOptions) => {
    const options = _.defaults(rawOptions, defaults.test);
    try {
      try {
        monkeypatchTestEnvironment(options);
      } catch (e) {
        console.error(e);
        throw e;
      }

      options.reportTitle = buildReportTitle(options);

      if (options.interactive) {
        try {
          setupInteractiveMode();
        } catch (e) {
          console.error(e);
          throw e;
        }
      } else if (options.build) {
        let mochaError;
        try {
          exec(`mocha --reporter mochawesome --reporter-options "reportPageTitle=MyIR UI Regression Tests,reportTitle=${options.reportTitle},reportFilename=index,inlineAssets=true" "${options.input}"`);
        } catch(e) {
          mochaError = e;
        }

        try {
          createCsvReport(options);
        } catch(e) {
          console.error('Error while creating CSV report:', e);
          throw e;
        }

  
        let exitCode = 0;
        do {
          try {
            exec(cmd);
          } catch(e) {
            exitCode = 1;
          }
        } while(exitCode === 0 && options.untilFailure);
        if (exitCode === 1) {
          process.exit(1);
        }
      }
    } catch(e) {
      process.exit(1);
    }
  });


program
  .command('deploy')
  .description('Deploys a report using surge.sh')
  .option('-d, --domain [url]', 'The website\'s domain. Defaults to myir-test-report.surge.sh')
  .option('-p, --project [path]', 'The path to the project. Defaults to ./mochawesome-report')
  .action((env, rawOptions) => {
    const options = _.defaults(rawOptions, defaults.deploy);
    try {
      exec(`surge --domain "${options.domain}" --project "${options.project}"`);
    } catch(e) {
      process.exit(1);
    }
  });

function generateApiDocumentation(options) {
  let fullHelp = ['# CLI'];
  try {
    if (options.output === undefined) {
      options.output = path.resolve(__dirname, '..', 'doc', 'CLI.md');
    }
    const cmd = `node ${path.resolve(__dirname, 'index.js')}`;
    fullHelp.push(
      execSync(`${cmd} --help`)
      .toString()
      .replace(/.*(\n|\r| )+Commands:(.|\n|\r)*/, '')
    );
    [
      'test',
      'deploy',
      'doc'
    ].forEach(sub => (fullHelp = fullHelp.concat([
      `## bin/index.js ${sub}`,
      execSync(`${cmd} ${sub} --help`).toString()
    ])));
    fs.writeFileSync(options.output, fullHelp.join('\n'));
  } catch(e) {
    console.error('There was an error building the help output.');
    process.exit(1);
  }
}



function generateDeveloperDocumentation(options) {
  try {
    if (options.output === undefined) {
      options.output = path.resolve(__dirname, '..', 'doc', 'DEVELOPER.md');
    }
    if (options.serve) {
      exec(`documentation serve --watch "${options.input}"`);
    } else {
      exec(
        `documentation build "${options.input}" --output "${options.output}" --format "md" && pandoc --wrap=preserve -f markdown_github -t "${options.to}" "${options.output}" -o "${options.output}"`
      )
    }
  } catch(e) {
    process.exit(1);
  }
}

program
  .command('doc')
  .description('Generates the developer/CLI documentation.')
  .option('--subcommands', 'Prints the documentation for subcommands. Invalidates all options below unless noted')
  .option('-i, --input [path]', 'The input path. Defaults to ./src')
  .option('-s, --serve', 'Run a hot-reloading preview on localhost:4001. Invalidates all options below')
  .option('-o, --output [path]', 'The output path. Defaults to doc/DEVELOPER.md normally, doc/CLI.md in subcommands mode, and doc/SCHEMA.md in schema mode.')
  .option('-t, --to [format]', 'The output format to pass to pandoc. Defaults to commonmark')
  .action((rawOptions) => {
    const options = _.defaults(rawOptions, defaults.doc);
    if (options.subcommands) {
      generateApiDocumentation(options);
    } else if (options.schema) {
      generateDataSchemaDocumentation(options);
    } else {
      generateDeveloperDocumentation(options);
    }
  });

// Initialization

// Attempt to monkeypatch the PATH
process.env.PATH = `${process.env.PATH || ''}:${path.resolve(__dirname, '..', 'node_modules', '.bin')}`

program.parse(process.argv);
