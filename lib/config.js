const _ = require('lodash');
const path = require('path');
const environment = require('./environment')();

const failedScreenshotPath = path.join(
  __dirname, '..', 'mochawesome-report', 'failed'
);

const config = Object.assign(
  {},
  environment,
  {
    shortTimeout: Number(environment.shortTimeout || 15000),
    timeout: Number(environment.timeout || 35000),
    longTimeout: Number(environment.longTimeout || 30000),
    failedScreenshotPath: (
      environment.failedScreenshotPath || failedScreenshotPath
    ),
    testPattern: new RegExp(environment.testPattern || '.'),
    // Split by comma, filter empty items
    chromeOpts: (environment.chromeOpts || '').split(',').filter(
      _.negate(_.isEmpty)
    ),
    enableDebug: environment.enableDebug || false
  }
);

module.exports = config;
