require('dotenv').config();

module.exports = () => ({
  shortTimeout: process.env.SHORT_TIMEOUT,
  timeout: process.env.TIMEOUT,
  longTimeout: process.env.LONG_TIMEOUT,
  failedScreenshotPath: process.env.FAILED_SCREENSHOT_PATH,
  dataPath: process.env.DATA_PATH,
  testPattern: process.env.TEST_PATTERN,
  enableDebug: process.env.ENABLE_DEBUG,
  untilFailure: process.env.UNTIL_FAILURE,
  chromeOpts: process.env.CHROME_OPTS
});
