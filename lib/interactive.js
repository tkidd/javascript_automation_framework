const path = require('path');
const fs = require('fs');

function enterInteractiveMode(driver) {
  process.on('uncaughtException', (err) => {
    console.error('Uncaught exception:', err);
  });

  require('./testUtils');
  let hasQuit = false;
  // TODO: everything here should technically go into lib/
  [
    require('../src/core'),
    require('../src/pages'),
    require('../src/flows'),
    require('../src/entities'),
    { data: require('../src/data') }
  ].forEach((src) => Object.assign(global, src));
  global.driver = driver;
  driver.onQuit_ = () => (hasQuit = true);
  const configPath = path.resolve(__dirname, '..', 'repl.js');
  if (fs.existsSync(configPath)) {
    require(configPath);
  }
  require('repl').start().on('exit', () => {
    if (!hasQuit) {
      driver.quit();
    }
  });
}

module.exports = enterInteractiveMode;
