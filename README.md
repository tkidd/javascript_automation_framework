# Basic Automation Framework

This is a basic javascript automation framework that can be modified to support web based applications

#How to run all tests

```
yarn build
```

#How to run specific tests

```
yarn build -i src/specs/CHANGEME.js
```

### Node Dependencies

```
yarn install
```

### External Dependencies

#### yarn

[Official documentation](https://yarnpkg.com/en/docs/install)


