const _ = require('lodash');
const Flow = require('./Flow');
const { GoogleSearchPage, URLPage } = require('../pages');


/**
 * Creates an Employer
 * 
 */
class GoogleSearchFlow extends Flow {
  constructor({ driver }) {
    super();
    this.driver = driver;
    this.googleSearchPage = new GoogleSearchPage({ driver });
    this.urlPage = new URLPage({ driver });

    _.bindAll(this, [
      'openWebpage',
      'searchInGoogle',
    ]);
  }

  openWebpage(url) {
    return this.urlPage.open(url)
  }

  //Goes to google and searches for whatever the search term is
  searchInGoogle(searchTerm) {
    return this.googleSearchPage.enterSearchTerm(searchTerm)
      .then(() => this.googleSearchPage.generalWait(200))
      .then(() => this.googleSearchPage.clickSearchButton())
  }

}

module.exports = GoogleSearchFlow;
