const _ = require('lodash');
const Page = require('./Page');

/**
 * The Google Search Page
 * @param options
 * @prop {Object} testCase - The test case data.
 * @prop {Object} driver - Driver via Selenium
 */
class GoogleSearch extends Page {
  constructor({ driver }) {
    super();
    this.driver = driver;
    this.selectors = {
      googleSearchField: By.name('q'),
      googleSearchButton: By.xpath(`(//*[@name='btnK'])[1]`)
    };

    _.bindAll(this, [
      'enterSearchTerm',
      'clickSearchButton'
    ]);
  }

  //Enters the search term into the search bar

  enterSearchTerm(searchTerm) {
    return this.findAndType(this.selectors.googleSearchField, searchTerm)
  }

  //Clicks the search button 

  clickSearchButton() {
    return this.findAndClick(this.selectors.googleSearchButton)
  }

}

module.exports = GoogleSearch;

