const _ = require('lodash');
const Page = require('./Page');

/**
 * Opens the google page.
 */
class URLPage extends Page {
  constructor({ driver }) {
    super();
    this.driver = driver;
    _.bindAll(this, [
      'open'
    ]);

  }

  /** Opens the login page. */
  open(url) {
    return this.driver.get(url)
    .then(() => this.generalWait(2000))
  }

}

module.exports = URLPage;
