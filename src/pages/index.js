module.exports = {
	Page: require('./Page'),
	GoogleSearchPage: require('./GoogleSearchPage'),
	URLPage: require('./UrlPage')
};
