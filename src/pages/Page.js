/**
 * Classes which follow the {@link https://martinfowler.com/bliki/PageObject.html Page Object} model.
 * The driver should not be accessed directly.
 * Instead, write classes which represent pages.
 */


module.exports = class Page {
    generalWait(waitTime) {
        return this.driver.wait(new Promise(resolve => (
            setTimeout(resolve, waitTime)
        )));
    }

    // Finds and element and clicks on it
    findAndClick(selector) {
        // Finds element
        return this.driver.wait(until.elementLocated(selector))
            .then((el) => {
                // Waits for element to be enabled and displayed, then clicks on it
                el.isEnabled()
                el.isDisplayed()
                el.click()
            })
            .catch(err => {
                console.log(err)
            })
    }

   
    // Finds an element and sends keys into it
    findAndType(selector, text) {
        // Finds element
        return this.driver.wait(until.elementLocated(selector))
            .then((el) => {
                // Waits for element to be enabled and displayed, then clears the field and types into it
                new Promise((resolve, reject) => {
                    try {
                        el.isEnabled()
                        el.isDisplayed()
                        el.clear()
                        resolve(el.sendKeys(text))
                    }
                    catch (err) {
                        console.log(err)
                        reject(err)
                    }

                })
            })
    }
}
