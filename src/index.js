require('../lib/testUtils');

describe('Google Search', () => {
	require('./specs/googleSearch');
});

describe('Basic API tests', () => {
	require('./specs/apiTests');
});

