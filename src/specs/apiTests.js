const chai = require('chai');
const expect = chai.expect;
const { APIInteractions } = require('../routes');
const { timeout } = require('../core/config');

const {
	defaultBeforeEach
} = require('../core/hooks');


describe('Basic REST Tests', function () {
	this.timeout(30000);

	before(async function () {
		apiInteractions = new APIInteractions();
	});

	beforeEach(function () {
		defaultBeforeEach(this);
	})

	after(async function () {
		this.timeout(timeout)
	});

	afterEach(function () {
		this.timeout(timeout);

	});

	it('should get list of users from reqres.in', async function () {
		try {
			response = await apiInteractions.basicGetRequest()
			expect(response.status).to.equal(200)
			expect(response.body.data[0].first_name).to.be.a('string').and.to.equal('Eve')
			console.log('Response: ', response.body)
		}
		catch (e) {
			console.log(response.body)
			console.warn(e)
			expect(e).to.not.exist()
		}
	})

	it('should create a user', async function () {
		try {
			response = await apiInteractions.basicPostRequest()
			expect(response.status).to.equal(201)
			expect(response.body.name).to.have.lengthOf(8).and.to.be.a('string').and.to.equal('morpheus')
			expect(response.body.job).to.be.a('string')
			expect(response.body.updatedAt).to.not.exist
			expect(response.body.createdAt).to.exist
			console.log('Response: ', response.body)
		}
		catch (e) {
			console.log(response.body)
			console.warn(e)
			expect(e).to.not.exist()
		}

	})

	it('should edit a user', async function () {
		try {
			response = await apiInteractions.basicPutRequest()
			expect(response.status).to.equal(200)
			expect(response.body.job).to.be.a('string').and.to.equal('zion resident')
			expect(response.body.updatedAt).to.exist
			console.log('Response: ', response.body)
		}
		catch (e) {
			console.log(response.body)
			console.warn(e)
			expect(e).to.not.exist()
		}
	})

	it('should delete a user', async function () {
		try {
			response = await apiInteractions.basicDeleteRequest()
			expect(response.status).to.equal(204)
			expect(response.body).to.be.empty
			console.log('Response: ', response.body)
		}
		catch (e) {
			console.log(response.body)
			console.warn(e)
			expect(e).to.not.exist()
		}
	})
});
