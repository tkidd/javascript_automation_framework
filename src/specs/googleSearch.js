const _ = require('lodash');
const chai = require('chai');
const expect = chai.expect;

const { timeout } = require('../core/config');
const {
	defaultBeforeAll, defaultAfterAll, defaultBeforeEach, defaultAfterEach
} = require('../core/hooks');
const Driver = require('../core/Driver');

const { GoogleSearchFlow } = require('../flows');

// Required data files
const urlDataGen = require('../data/testData/url/urlDataGen')
const searchDataGen = require('../data/testData/search/searchDataGen')
const urlSpreadsheetGen = require('../data/dataGenScripts/urlSpreadsheetGen')
const parseData = require('../data/dataGenScripts/csvRead')


describe(`Google Searches`, function () {
	let suite = this;
	this.timeout(70000);

	before(async function () {
		defaultBeforeAll(suite, this);
		driver = Driver();
		googleSearchFlow = new GoogleSearchFlow({ driver });

		urlData = await urlDataGen()
		searchData = await searchDataGen()
		await urlSpreadsheetGen(1)


	});

	beforeEach(function () {
		defaultBeforeEach(this);
	});

	after(function () {
		this.timeout(timeout);
		defaultAfterAll(suite, this);
		return driver.quit();
	});

	afterEach(function () {
		this.timeout(timeout);
		return defaultAfterEach(this, driver, {});
	});


	specify('opens webpage and navigates to google', function () {
		let url = urlData.url

		this.timeout(timeout);
		return googleSearchFlow.openWebpage(url)
	});

	specify('searches for STC and clicks search', function () {
		let searchTerm = searchData.searchTerm

		this.timeout(timeout);
		return googleSearchFlow.searchInGoogle(searchTerm);
	});

	specify('generates data onto a csv, reads that data and searches with it', async function () {
		csvData = await parseData()
		let url = csvData[0]

		this.timeout(timeout)
		return googleSearchFlow.openWebpage(url)


	})
});
