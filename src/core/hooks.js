const _ = require('lodash');
const path = require('path');
const fs = require('fs');
const { sync: mkdirp } = require('mkdirp');
const addContext = require('mochawesome/addContext');
const {
	failedScreenshotPath: folder, testPattern, enableDebug
} = require('./config');

let debugging = false;



function filterEach(that) {
	if (testPattern.exec(that.currentTest.fullTitle()) === null) {
		if (that.skip) {
			that.skip();
		}
		return true;
	}
	return false;
}

/**
 * Skips a test by searching its full title for the configured pattern.
 * @returns {boolean} - Whether it was skipped.
 */
function filterBeforeEach(that) {
	return filterEach(that);
}

/**
 * Skip the rest of the hook if the test was filtered.
 * @returns {boolean} - Whether it was skipped.
 */
function filterAfterEach(that) {
	return filterEach(that);
}

function filterAll(suite, that) {
	const shouldSkip = !suite.tests.some(
		test => testPattern.exec(test.fullTitle())
	);
	if (shouldSkip && that.skip) {
		that.skip();
	}
	return shouldSkip;
}

/**
 * Skips a suite by searching each test full title for the configured pattern.
 * @returns {boolean} - Whether the suite was skipped.
 */
function filterBeforeAll(suite, that) {
	return filterAll(suite, that);
}

/**
 * Skips the after hook of a suite if each test title does not match the configured pattern.
 * @returns {boolen} - Whether the hook was skipped.
 */
function filterAfterAll(suite, that) {
	return filterAll(suite, that);
}

function debugHook(that) {
	if (!enableDebug) {
		return false;
	}
	if (debugging) {
		that.skip();
	}
	return debugging;
}

/**
 * Enters into interactive mode if the current test failed and debugging is enabled.
 *
 * If debug mode has already been enabled, the rest of the hook will be skipped.
 *
 * @param {Object} helpers
 * Instances to assign to global.
 * In debug interactive mode, loginFlow is not created and should be provided.
 *
 * @returns {Boolean} debugging
 */
function debugAfterEach(that, driver, helpers = {}) {
	if (!enableDebug) {
		return false;
	}
	if (debugging) {
		that.skip();
	} else if (that.currentTest.state === 'failed') {
		debugging = true;
		Object.assign(global, helpers);
		require('../../lib/interactive')(driver);
	}
	return debugging;
}

/**
 * Skips the rest of the hook if currently in debugging mode.
 *
 * @returns {Boolean} debugging
 */
function debugBeforeEach(that) {
	return debugHook(that);
}

/**
 * Skips the rest of the hook if currently in debugging mode.
 *
 * @returns {Boolean} debugging
 */
function debugBeforeAll(that) {
	return debugHook(that);
}

/**
 * Skips the rest of the hook if currently in debugging mode.
 *
 * @returns {Boolean} debugging
 */
function debugAfterAll(that) {
	return debugHook(that);
}

/**
 * Saves a screenshot to the configured failure screenshot folder if a test failed.
 */
function screenshotAfterEach(that, driver) {
	const test = that.currentTest;
	if (test.state === 'failed') {
		mkdirp(folder);
		let filename = `${new Date()} ${test.fullTitle()}.png`;
		filename = filename.replace(/\/|\\/g, ' ');
		return driver.takeScreenshot()
			.then((image) => {
				try {
					fs.writeFileSync(path.join(folder, filename), image, 'base64');
					addContext(that, {
						title: 'Screenshot at the time of failure',
						value: _.escape(`failed/${filename}`)
					});
				} catch (e) {
					console.error('Error taking screenshot', filename);
					console.error(e);
				}
			});
	}
	return Promise.resolve();
}

function closeBrowserOnFailure(that, driver) {
	const test = that.currentTest;
	if (test.state === 'failed') {
		return this.timeout(5000)
			.then(() => driver.quit());
	}
	return Promise.resolve();
}

/**
 * Adds performance logs to failed test context.
 */
function performanceLogsAfterEach(that, driver) {
	const test = that.currentTest;
	if (test.state === 'failed') {
		return driver.executeScript('return window.performance.getEntries();')
			.then(logs => addContext(that, {
				title: 'Performance Logs',
				value: JSON.stringify(logs, null, 2)
			}))
			.catch(e => addContext(that, {
				title: 'Performance Logs',
				value: `Could not retrieve logs: ${e.message}`
			}));
	}
	return Promise.resolve();
}

/**
 * A composite of all other before hooks.
 */
function defaultBeforeAll(suite, that) {
	filterBeforeAll(suite, that);
	debugBeforeAll(that);
}

/**
 * A composite of all other after hooks.
 */
function defaultAfterAll(suite, that) {
	filterAfterAll(suite, that);
	debugAfterAll(that);
}

/**
 * A composite of all other before each hooks.
 */
function defaultBeforeEach(that) {
	filterBeforeEach(that);
	debugBeforeEach(that);

}

/**
 * A composite of all other after each hooks.
 */
function defaultAfterEach(that, driver, helpers) {
	const promises = [];
	filterAfterEach(that);
	promises.push(screenshotAfterEach(that, driver));
	promises.push(performanceLogsAfterEach(that, driver));
	debugAfterEach(that, driver, helpers);
	return Promise.all(promises);
}

module.exports = {
	filterBeforeEach,
	filterBeforeAll,
	filterAfterAll,
	debugBeforeEach,
	debugAfterEach,
	debugAfterAll,
	screenshotAfterEach,
	defaultBeforeAll,
	defaultAfterAll,
	defaultBeforeEach,
	defaultAfterEach
};
