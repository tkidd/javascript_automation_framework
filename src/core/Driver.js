const webdriver = require('selenium-webdriver');
const { Options } = require('selenium-webdriver/chrome');
const { chromeOpts } = require('./config');


global.By = webdriver.By;
global.until = webdriver.until;
global.Key = webdriver.Key;

const caps = {
  'browserName': 'chrome',
  'version': 'latest',
  'video': 'True'
}

const options = new Options();
chromeOpts.forEach(opt => options.addArguments(opt));
// Headless is required to run tests on jenkins. Add '--chrome-opts headless' in the command line to run in headless.
options.addArguments('disable-infobars', 'window-size=1920,1080', 'incognito', 'disable-gpu');

/**
 * @name Driver
 * @description
 * Creates a new preconfigured selenium driver.
 */
module.exports = () => new webdriver.Builder()
  // This is for gridlastic if we ever decide to go with that
  //.usingServer('http://7AS1S2kwYTnNnSyxjsnN4mL50wN0h2Le:tt6bmhya0LOeucCDvQgq2SlTeZJlRlqb@14TWCDX4.gridlastic.com:80/wd/hub')
  .withCapabilities(caps)
  .setChromeOptions(options)
  .build();

