const request = require('supertest');
const _ = require('lodash');
const APIBase = require('./APIBase');

class APIInteractions extends APIBase {
    constructor() {
        super();
        this.endpoint = {
            get: '/users?page=2',
            post: '/users',
            put: '/users/2',
            delete: '/users/2'

        };
        _.bindAll(this, [
            'basicGetRequest',
            'basicPostRequest',
            'basicPutRequest'
        ]);
    }


    async basicGetRequest() {
        console.log(`Getting list of users from reqres.in`)
        let response = await request(this.apiURL)
            .get(this.endpoint.get)
            
        return response
    }

    async basicPostRequest() {

        let basicPostBody = {
            name: 'morpheus',
            job: 'leader'
        }

        console.log('Request: ', basicPostBody)
        let response = await request(this.apiURL)
            .post(this.endpoint.post)
            .send(basicPostBody)

        return response
    }


    async basicPutRequest() {

        let basicPutBody = {
            name: 'morpheus',
            job: 'zion resident'
        }

        console.log('Request: ', basicPutBody)
        let response = await request(this.apiURL)
            .put(this.endpoint.put)
            .send(basicPutBody)

        return response
    }

    async basicDeleteRequest() {
        let response = await request(this.apiURL)
            .delete(this.endpoint.delete)

        return response
    }



}

module.exports = APIInteractions