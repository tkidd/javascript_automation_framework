const SearchData = require('./SearchData')
const searchData = new SearchData

const searchDataGen = async function () {
    console.log(`Generating search data`)
    data = await searchData.createSearchData()
    console.log(data)
    return data
}

module.exports = searchDataGen