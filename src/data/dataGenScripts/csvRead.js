const csv = require('fast-csv')

async function getData() {
    return new Promise((resolve) => {
        csv.fromPath('./src/data/dataGenScripts/url_data.csv')
            .on('data', function (data) {
                resolve(data)
            })
    })
}

async function parseData() {
    let data = await getData()
    let parsedData = data.toString()
    var poctURL = parsedData.split('|')[0]
    var stcURL = parsedData.split('|')[1]

    return [poctURL, stcURL]

}

module.exports = parseData

