const fs = require('fs');
const URLSpreadsheet = require('./urlSpreadsheet');

const urlSpreadsheet = new URLSpreadsheet();
const csv = fs.createWriteStream('./src/data/dataGenScripts/url_data.csv');


async function* dataGenerator(length) {
    for (let i = 0; i < length; i++) {
        let row = await urlSpreadsheet.searchData()
        yield row;
    }
}

async function main(length) {
    const data = dataGenerator(length);
    for await (let array of data) {
        let pipeDelimitedArray = array.join('|')
        console.log(pipeDelimitedArray)
        csv.write(`${pipeDelimitedArray}\n`);

    }
    await console.log('File created!')
}

module.exports = main;



