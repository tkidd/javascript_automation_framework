class URLSpreadsheet {

    poctURL() {
        return 'https://walmart.poct-test.stchome.net'
    }

    stcURL() {
        return 'https://stchealth.com'
    }

    searchData() {
        let dataArray = [];

        dataArray.push(this.poctURL());
        dataArray.push(this.stcURL());
        
        return Promise.all(dataArray);
    }
}


module.exports = URLSpreadsheet;

