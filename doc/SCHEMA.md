# Test Data Schema

```
{
  "instances": [
    {
      "instanceMeta": "string",
      "url": "string",
      "users": [
        {
          "userMeta": "string",
          "user": {
            "isAdmin": [
              true,
              false
            ],
            "username": "string",
            "password": "string"
          },
          "orgFacs": [
            {
              "orgFacMeta": "string",
              "orgFac": {
                "vfcPin": "string",
                "isOrg": [
                  true,
                  false
                ]
              },
              "orders": [
                {
                  "orderMeta": "string",
                  "orderSet": {
                    "name": "string"
                  },
                  "items": [
                    {
                      "itemMeta": "string",
                      "vaccine": {
                        "name": "string",
                        "type": "string",
                        "ndc": "string",
                        "fundingSource": "string",
                        "minQuantity": 0,
                        "maxQuantity": 0
                      },
                      "quantity": {
                        "invalid": [
                          {
                            "quantityMeta": "string",
                            "amount": 0
                          }
                        ],
                        "valid": {
                          "noWarning": [
                            {
                              "quantityMeta": "string",
                              "amount": 0
                            }
                          ],
                          "final": {
                            "quantityMeta": "string",
                            "amount": 0
                          }
                        }
                      }
                    }
                  ],
                  "priorityInfo": {
                    "invalid": [
                      {
                        "priorityMeta": "string",
                        "status": [
                          true,
                          false
                        ],
                        "reason": [
                          false,
                          "string"
                        ],
                        "comment": [
                          false,
                          "string"
                        ]
                      }
                    ],
                    "valid": {
                      "noWarning": [
                        {
                          "priorityMeta": "string",
                          "status": [
                            true,
                            false
                          ],
                          "reason": [
                            false,
                            "string"
                          ],
                          "comment": [
                            false,
                            "string"
                          ]
                        }
                      ],
                      "final": {
                        "priorityMeta": "string",
                        "status": [
                          true,
                          false
                        ],
                        "reason": [
                          false,
                          "string"
                        ],
                        "comment": [
                          false,
                          "string"
                        ]
                      }
                    }
                  },
                  "shipping": {
                    "invalid": [
                      {
                        "shippingMeta": "string",
                        "instructions": [
                          false,
                          "string"
                        ],
                        "temporary": [
                          false,
                          {
                            "monday": [
                              "0000",
                              "2400"
                            ],
                            "tuesday": [
                              "0000",
                              "2400"
                            ],
                            "wednesday": [
                              "0000",
                              "2400"
                            ],
                            "thursday": [
                              "0000",
                              "2400"
                            ],
                            "friday": [
                              "0000",
                              "2400"
                            ],
                            "saturday": [
                              "0000",
                              "2400"
                            ],
                            "sunday": [
                              "0000",
                              "2400"
                            ],
                            "expiresIn": 0
                          }
                        ],
                        "permanent": [
                          false,
                          {
                            "monday": [
                              "0000",
                              "2400"
                            ],
                            "tuesday": [
                              "0000",
                              "2400"
                            ],
                            "wednesday": [
                              "0000",
                              "2400"
                            ],
                            "thursday": [
                              "0000",
                              "2400"
                            ],
                            "friday": [
                              "0000",
                              "2400"
                            ],
                            "saturday": [
                              "0000",
                              "2400"
                            ],
                            "sunday": [
                              "0000",
                              "2400"
                            ]
                          }
                        ]
                      }
                    ],
                    "valid": {
                      "final": {
                        "shippingMeta": "string",
                        "instructions": [
                          false,
                          "string"
                        ],
                        "temporary": [
                          false,
                          {
                            "monday": [
                              "0000",
                              "2400"
                            ],
                            "tuesday": [
                              "0000",
                              "2400"
                            ],
                            "wednesday": [
                              "0000",
                              "2400"
                            ],
                            "thursday": [
                              "0000",
                              "2400"
                            ],
                            "friday": [
                              "0000",
                              "2400"
                            ],
                            "saturday": [
                              "0000",
                              "2400"
                            ],
                            "sunday": [
                              "0000",
                              "2400"
                            ],
                            "expiresIn": 0
                          }
                        ],
                        "permanent": [
                          false,
                          {
                            "monday": [
                              "0000",
                              "2400"
                            ],
                            "tuesday": [
                              "0000",
                              "2400"
                            ],
                            "wednesday": [
                              "0000",
                              "2400"
                            ],
                            "thursday": [
                              "0000",
                              "2400"
                            ],
                            "friday": [
                              "0000",
                              "2400"
                            ],
                            "saturday": [
                              "0000",
                              "2400"
                            ],
                            "sunday": [
                              "0000",
                              "2400"
                            ]
                          }
                        ]
                      }
                    }
                  }
                }
              ]
            }
          ]
        }
      ]
    }
  ]
}
```