# Contributing

## Getting Started

- Read the [scripts](SCRIPTS.md) guide
- Read the [CLI help](CLI.md)
- Read the [test data schema](SCHEMA.md)
- Read the output of `node bin/index.js --help`
- Read the [developer documentation](DEVELOPER.md)
- Read the rest of this file

## External Documentation

- Documentation format: [JSDoc syntax](http://usejsdoc.org/)
- Documentation system: [Documentation.js](https://github.com/documentationjs/documentation/blob/master/docs/GETTING_STARTED.md)
    * [Styleguide/Troubleshooting](https://bitbucket.org/stchome/node-doc/src/master/styleguides/DOCUMENTATIONJS.md)
- Test runner: [mocha](https://mochajs.org/)
- Assertion: [chai](http://chaijs.com/api/bdd/)
- Browser control: [selenium-webdriver](http://www.seleniumhq.org/docs/03_webdriver.jsp) (enable `(javascript)` and disable `(java)` at the top of the page)
    * [Full API docs](http://seleniumhq.github.io/selenium/docs/api/javascript/module/selenium-webdriver/)
    * [xpath guide](https://www.guru99.com/xpath-selenium.html)
- [Page Object guide](https://martinfowler.com/bliki/PageObject.html)
- [Promise cheat sheet](https://retrocadenet.github.io/Promise-cheatsheet/)

## Basics

When creating any new file, add it to the `index.js` file in its folder.

Bind class methods using [bindAll](https://lodash.com/docs/4.17.4#bindAll) so that tests are not impacted by the scope of the function.
Classes should extend their base class.
Pages extend Page, flows extend Flow, entities extend Entity.

```
const _ = require('lodash');
class MyClass extends BaseClass {
  constructor() {
    super();
    _.bindAll(this, 'myMethod');
  }

  myMethod() {
    // ...
  }
}
```

Flows and pages should take the driver in as a parameter.
They should also take in the current test case (the flattened test data for the current suite) to use its URL.
When pages or flows need an instance of a page, flow, or entity, they should initialize them in their constructor.

```
class MyFlow extends Flow {
  constructor({ driver, testCase }) {
    super();
    this.driver = driver;
    this.testCase = testCase;
    this.loginFlow = new LoginFlow({ driver });
  }
}
```

When using the driver inside a class member, always return the promise returned from the driver so it can be waited on.
If you need to return some other value, finish the promise first.

```
  // do not do it this way
  wrongMethod(valueToReturn) {
    return this.driver.findElement(this.selectors.someSelector)
      .then(el => {
        el.doSomething();
        return valueToReturn;
      });
  }

  // do it this way
  rightMethod(valueToReturn) {
    return this.driver.findElement(this.selectors.someSelector)
      .then(el => el.doSomething())
      .then(() => valueToReturn)
  }
```

[By](http://seleniumhq.github.io/selenium/docs/api/javascript/module/selenium-webdriver/index_exports_By.html),
[Key](http://seleniumhq.github.io/selenium/docs/api/javascript/module/selenium-webdriver/index_exports_Key.html),
and [until](http://seleniumhq.github.io/selenium/docs/api/javascript/module/selenium-webdriver/lib/until.html)
from webdriver are all available globally.

Always document classes and their methods using JSDoc, then run `yarn doc`.
Various configuration is available from `src/core/config.js`.
See [Getting Started](#markdown-header-getting-started) for more.

Finally, whenever establishing new standards and patterns, bring them up to the team and document them in this file.

## Page Objects

This repository follows the PageObject model.
Instead of using the webdriver to directly access elements, write objects representing pages.
This allows us to be flexible when the UI changes.
Avoid using selectors directly outside of page objects.

Always include [wait methods](https://watirmelon.blog/2015/11/04/waiting-in-webdriverjs/) in class objects so that flows can wait for elements to appear.
Test your page objects using the `repl` subcommand to ensure that it can be used.
Chain operations inside the page to ensure that the object can be used without manually waiting for anything.
This ensures that the page object can be used inside of tests.

Page objects don't need to be written in their entirety when they are first needed.
Instead, write enough of the object to complete the tests.

When needing to reuse selectors within an object, contain them in a `this.selectors` plain object inside of the constructor.

```
class MyPage extends Page {
  constructor({ driver }) {
    super();
    this.driver = driver;
    this.selectors = {
      myElement: By.xpath('//*[@class='my__class']')
    };
  }
}
```

Certain selectors may need to be shared globally.
These are available in `src/selectors`.

There should be a method called `open` that opens the browser to the page if a direct route exists.
The `url-join` library is included as a convenience.

```
const urlJoin = require('url-join');
const { url } = require('../core/config');
class MyPage extends Page {
  // ...

  open() {
    return this.driver.get(urlJoin(url, `/my-page`));
  }
}
```

Page objects should also have methods that wait for elements and find elements.
These should be named `waitFor*` or `waitUntil*` and `find*`.

Page objects may have composite methods that allow them to perform basic operations within the page.
Avoid chaining too many actions together, as this is the purpose of flows.

Page objects may have composite methods that enter input.

Page objects may have methods that require multiple pieces of information to find a specific element or text.

Pages may need collections of elements to find items on the page.
These should not be entities, but instead should be instance members.

```
class MyPage extends Page {
  constructor({ driver }) {
    super();
    this.inputs = {
      someElement: { label: 'Some label', input: 'textbox' },
      someOtherElement: { 'Some other label', input: 'checkbox' }
    };
  }
}
```

Some entities require multiple pieces of information from a page to be properly instantiated. Since chaining multiple actions within a page should be avoided, entity creation should generally be left to flows instead.

Furthermore, pages should not depend on entities for execution of their methods if they don't themselves provide a way of instantiating the needed entities themselves. This would force users to instantiate their own entities in order to be able to use the page objects, or force dependency upon a flow capable of instantiating the entity.
If a page object doesn't provide methods for instantiating entities, those same entities should not be required in order to execute page object methods. Use strings or plain objects instead.

***WebElement Methods***

Avoid using [WebElement methods](http://seleniumhq.github.io/selenium/docs/api/javascript/module/selenium-webdriver/lib/webdriver_exports_WebElement.html) such as `click`, `sendText`, and `clear` directly on `find*` page methods.
Instead, use `.then` to apply changes.
This is because it cannot be guaranteed that a WebElement promise is returned directly from page object class methods.

```
// Fails if the method's return value is chained
flow.page.findSomeElement().click()

// Works even if the implementation changes
flow.path.findSomeElement().then(el => el.click())
```

***Xpaths***

When writing xpaths for selenium, you may test them in the browser console.
[Read more here](https://stackoverflow.com/questions/22571267/how-to-verify-an-xpath-expression-in-chrome-developers-tool-or-firefoxs-firebug).
Always make sure an xpath does not match more than one element on a page.

If changes in the page are too quick, you can cause the browser to pause on any changes.
Enter in the element inspector, right click on the root element where the changes take place, and select `Break On > Subtree Changes`.

Try to tie xpaths to identifiers like classnames, ids, and data attributes.
Try to avoid using nesting and labels to identify elements whenever possible.

When developing new pages, try to add data attributes to components to easily identify them.

## Flows

MyIR features many sequences of actions throughout pages to perform essential actions.
We use objects that represent these flows that contain many page objects, and may even contain other flows.
Use flow objects to represent workflows that users perform, not necessarily sequences of actions targetted towards any indivial tests.

Name flow classes with `*Flow`.

Flow method names should represent what action they are performing from a user's perspective, not what they are doing with page objects.
Flow methods should complete full steps of workflows.
They should bring the application to a useful point where other actions can take place.

Flows, should not directly take in input, but should take in entities where possible.
Flows should populate the members of entities as they get more information about the entity while traversing the workflow.

```
class MyFlow extends Flow {
  constructor({ driver }) {
    super();
    this.myPage = new MyPage({ driver });
  }

  selectSomething(somethingEntity) {
    return this.myPage.selectSomethingUsingId(somethingEntity)
      .then(() => this.myPage.waitForSomethingToFinish());
  }
}
```

## Entities

There are many collections of data used in MyIR with many different properties.
To simplify in keeping these properties together, and to standardize the interface to this data, we group them together in class instances.

Avoid using strings of data directly, and instead keep them in entities.
Entities should provide methods to set their data.
Entity set methods allow for deleting properties, transforming arguments, and type checking.
Entities should provide their data as properties.
Entities should provide methods to make copies of themselves so that global entities are not mutated.
Entity constructors should be able to make copies of other entities of the same kind.

```
const newEntity = new MyEntity(oldEntity);

newEntity.isEq(oldEntity);
// => true
newEntity.setValue(someValue);
newEntity.value;
// => someValue
newEntity.isEq(oldEntity);
// => false

newEntity.hasOwnProperty('value');
// => true
newEntity.setValue(undefined);
newEntity.hasOwnProperty('value');
// => false
```

## Configuration

The program should be configurable through CLI options and environment variables.
New configuration should be added in `bin/index.js` in the function `monkeypatchTestEnvironment` and the option chain for the `test` subcommand.
See [the commander package](https://www.npmjs.com/package/commander) for more information.
New configuration also needs to be added to `lib/environment.js`.
New environment variables must be documented in [the CLI documentation](SCRIPTS.md).

## Tests

***Hooks***

Test suites should not rely on eachother.
Instead, use flows to set up test prerequisites in the `before` [hook](https://mochajs.org/#hooks)s of tests.
Never put prerequisites for tests inside of tests, because when a test pattern is specified, they will not be run.

Do not initialize the driver inside the test suite, and instead initialize it in the `before` hook.
Otherwise, an extra instance of the driver will be created before the tests begin.

Use all `default*` hooks unless if there is some special case.
Log out and close the driver in the `after` hook.

Include extra flows, pages, and entities in the `defaultAfterEach` hook to make them available in `--enable-debug` mode.

```
describe('My Workflow', function () {
  const suite = this;
  let driver;
  let loginFlow;
  let orgFacFlow;
  let myFlow;

  before(function () {
    this.timeout(longTimeout);
    defaultBeforeAll(suite, this);
    driver = Driver();
    loginFlow = new LoginFlow({ driver });
    orgFacFlow = new OrgFacFlow({ driver });
    myFlow = new MyFlow({ driver });
    return loginFlow.login(user)
      .then(() => myFlow.initBasicOrderWorkflow)
      .then(() => orgFacFlow.selectOrgFac(orderFac.vfcPin));
  });

  beforeEach(function () {
    defaultBeforeEach(this);
  });

  after(function () {
    this.timeout(timeout);
    defaultAfterAll(suite, this);
    return loginFlow.logout()
      .then(() => driver.quit());
  });

  afterEach(function () {
    this.timeout(timeout);
    return defaultAfterEach(
      this, driver, { loginFlow, , orgFacFlow, myFlow }
    );
  });
});
```

***Data for Tests***

End-to-end tests should be partially data driven.
Please read what data driven tests are [here](https://support.smartbear.com/testcomplete/docs/testing-with/data-driven/basic-concepts.html).
In summary, tests should not rely on one set of data, but should be fed many sets of data from some configuration.
In end to end tests, all information that can be verified across sections in the UI should be.

Data is fed to our tests through `testData.json`.
See [the schema outline](SCHEMA.md) for more.
New sections of the schema need to be outlined in `lib/testDataSchema.js`.

Once the data is specified, it should be deeply flattened.
Each test suite should correspond to one object.
This allows iterating over each test case rather than using nested loops.
See `src/data/index.js`.

Each test suite and test should have associated metadata which is used in the description.
Tests should often include multiple failing cases.

***Debugging Failed Tests***

When end-to-end tests fail intermitently, it's most often because something isn't being waited on properly.
See [the selenium docs for wait conditions](http://seleniumhq.github.io/selenium/docs/api/javascript/module/selenium-webdriver/lib/until.html).
`until.elementIsEnabled` should be used to wait until elements are enabled, for example.

To debug failed tests, use the `--enable-debug` mode to drop into interactive mode on failure.
Use `--until-failure` to run tests until they fail.
Use `--test-pattern` to specify which tests to run.
You can use regex to specify the pattern.
See the [regex quickstart guide](http://www.rexegg.com/regex-quickstart.html) for more.

## Examples

- Page Object:
  - [DefineOrderSet](../src/pages/DefineOrderSet.js)
- Page Flow:
  - [OrderFlow](../src/pages/DefineOrderSet.js)
- Entity:
  - [User](../src/entities/User.js)
