# Scripts

## Building

`yarn build`

Runs the tests once and creates a deployable html report at `mochawesome-report`.

`yarn dev`

Runs the tests in watch mode.

## Deploying

`yarn deploy`

Deploy the report to `surge.sh`.

See [the CI documentation for more](./CI.md).

## Documentation

`yarn doc`

Update the developer documentation.

`yarn doc:cli`

Update the CLI documentation.

`yarn doc:schema`

Update the schema documentation.

`yarn doc:watch`

Run a hot-reloading documentation preview on localhost:6060.

## CLI

The interface is used internally by scripts.
You can run the command line interface with `node bin/index.js`.

Running the CLI directly requires that `node_modules/.bin` is in your `PATH`.
It will attempt to monkeypatch the `PATH`.

For more information, run `node bin/index.js --help`
or `node bin/index.js [command] --help`
or view [the full help](CLI.md).

### Configuration

The `test` command can be configured (in order of priority)
through arguments, environment variables, or the `.env` file.

option                    | environment variable        |
--------------------------|-----------------------------|
--chrome-opts             | CHROME\_OPTS                |
--enable-debug            | ENABLE\_DEBUG               |
--until-failure           | UNTIL\_FAILURE              |
--short-timeout           | SHORT\_TIMEOUT              |
--timeout                 | TIMEOUT                     |
--long-timeout            | LONG\_TIMEOUT               |
--failed-screenshot-path  | FAILED\_SCREENSHOT\_PATH    |
--test-pattern            | TEST\_PATTERN               |
--csv-report              | CSV\_REPORT                 |
--report-title            | REPORT\_TITLE               |

***Test Data***

The `test` command also requires that a `testData.json` file is populated.
The file is used to specify test cases.
See [the schema outline](SCHEMA.md) for the required shape of the file.
