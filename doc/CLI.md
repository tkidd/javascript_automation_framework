# CLI

  Usage: index [options] [command]


  Options:

    -V, --version  output the version number

## bin/index.js test

  Usage: test [options]

  Runs the tests through mocha.


  Options:

    -i, --input [path]               The test entrypoint. Defaults to ./src/index.js
    --chrome-opts [options]          An array of comma-delimited extra options to pass to chromedriver.
    --interactive                    Instead of running tests, enters into an interactive mode with the test library imported and logged in. Invalidates all options below
    --enable-debug                   Enable pausing and entering interactive mode when a test fails. Prevents mocha from exiting if there are any pending operations
    --short-timeout [ms]             How long to wait for short tests
    --timeout [ms]                   How long to wait for average tests
    --long-timeout [ms]              How long to wait for long tests
    --failed-screenshot-path [path]  The path of the folder to save screenshot failures. Defaults to ./mochawesome-report/failed
    --test-pattern [regex]           A pattern used to filter full test names (including the suite names)
    --report-title                   The report title to use in --build mode. Defaults to "MyIR UI Regression Tests: (git info)"
    --csv-report [path]              The path to export a CSV report in --build mode. Defaults to ./report.csv
    -d, --data-path [path]           The path to the test data. Defaults to ./testData.json
    --build                          Build a deployable html report. Invalidates all options below
    --no-watch                       Do not watch the source files for changes
    --until-failure                  Runs the tests until they fail. This disables watch mode.
    -r, --reporter [mocha reporter]  The reporter to use. Passed to mocha if specified
    -h, --help                       output usage information

## bin/index.js deploy

  Usage: deploy [options]

  Deploys a report using surge.sh


  Options:

    -d, --domain [url]    The website's domain. Defaults to myir-test-report.surge.sh
    -p, --project [path]  The path to the project. Defaults to ./mochawesome-report
    -h, --help            output usage information

## bin/index.js doc

  Usage: doc [options]

  Generates the developer/CLI documentation.


  Options:

    --subcommands        Prints the documentation for subcommands. Invalidates all options below unless noted
    --schema             Prints the documentation for the test data schema. Invalidates all options below unless noted
    -i, --input [path]   The input path. Defaults to ./src
    -s, --serve          Run a hot-reloading preview on localhost:4001. Invalidates all options below
    -o, --output [path]  The output path. Defaults to doc/DEVELOPER.md normally, doc/CLI.md in subcommands mode, and doc/SCHEMA.md in schema mode.
    -t, --to [format]    The output format to pass to pandoc. Defaults to commonmark
    -h, --help           output usage information
