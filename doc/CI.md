# Integrating with CI software

## Running tests behind a firewall

Many MyIR environments are protected behind a firewall.
Follow this section to run tests on these environments.

### With Cisco AnyConnect

To use Cisco AnyConnect on a CI server,
you must use the open source OpenConnect client.

On Jenkins, you can use
[the OpenConnect plugin](https://wiki.jenkins.io/display/JENKINS/OpenConnect+Plugin).
You must follow the instructions for the prerequisites.

#### Building From Source

If the plugin is not available on your Jenkins instance,
you may need to build it from soure.

```
git clone https://github.com/jenkinsci/openconnect-plugin \
  && cd openconnect-plugin \
  && mvn package
  && ls target/openconnect.hpi
```

Navigate to `Configure > Plugins > Advanced > Upload`
and upload the `.hpi` file.

## Dependencies

### Node Dependencies

You muse run `yarn install` during the build process.

### External Dependencies

On Jenkins, Node can be installed via the
[Node plugin](https://wiki.jenkins.io/display/JENKINS/NodeJS+Plugin).

You must install chromedriver normally on the Jenkins instance.
The chromedriver executable must be in the `PATH` when tests run
(`/usr/lib/chromium-browser` on ubuntu).

You must also use the
[Xvfb Plugin](https://wiki.jenkins.io/display/JENKINS/Xvfb+Plugin).
Xvfb needs to be installed and configured at
`Configure > Global Tool Configuration`.
The Xvfb display number needs to be set inside of the job configuration.

Preliminary test runs of 3rd party test runner are going to be routed
through Gridlastic. There is no need to install Chromedriver on the Jenkins
instance or the Xvfb plugin. The test runs will be configured through
environment variables on the test command. `SELENIUM_BROWSER=$browserName:$browserVersion:$platform`,
`SELENIUM_REMOTE_URL=$gridlasticHubEndpoint`. These will override the default values.

## Building the report

You must run `yarn build` during the build process,
or run `bin/index.js test --build` in order to pass in more arguments.

See `bin/index.js test --help` for more information
or view [the full help](CLI.md).

### Configuration

Depending on the power of the server, you can use the
`SHORT_TIMEOUT`, `TIMEOUT`, and `LONG_TIMEOUT`
environment variables to configure test time.

See [script documentation](SCRIPTS.md) for information on required variables.

To run tests, you also need a `testData.json` file.
See [the schema outline](SCHEMA.md) for the required shape of the file.

#### Adding more information to the report

mochawesome
[currently only supports](https://github.com/adamgruber/mochawesome/issues/178)
setting global report information through the title.

You can configure the report title through
the `REPORT_TITLE` environment variable or the `--report-title` argument.

It's recommended that you include the URL and git commit to the title.
The date the test was run is already available.


## Configuring fail state

Typically, the pipeline for tests should not fail even if the tests failed.
Reports still must be deployed even if not every test was successful.

In the shell step of your command line,
you can use the following command to fail only if the report did not build.

```sh
node "$WORKSPACE/bin/index.js" test --build || true
[ -e mochawesome-report/index.html ]
```

## Additional Jenkins plugins

To archive the test reports, you may want to use the
[HTML Publisher Plugin](https://wiki.jenkins.io/display/JENKINS/HTML+Publisher+Plugin).

HTML reports require that scripts can run inside the report.
To enable reports, navigate to the script console and run
`System.setProperty("hudson.model.DirectoryBrowserSupport.CSP", "")`.
To understand the full implications of this command, see
[the Jenkins documentation](https://wiki.jenkins.io/display/JENKINS/Configuring+Content+Security+Policy).

A CSV report is saved at the path specified by `--csv-report` while running
`bin/index.js test --build`. This report can be used to plot test stats with the
[Plot Plugin](https://wiki.jenkins.io/display/JENKINS/Plot+Plugin).
By default, this exists at `report.csv`.
